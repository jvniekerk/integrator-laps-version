﻿Option Explicit On
Imports System.Data.SqlClient

Module modSqlConnections

    Function GetConnectionString(Optional ByVal connType As String = "replicated") As String
        'Dim connString As String = "Server=172.27.63.16;Database=Automation;User Id=SA;Password=u3zD2Ix6"
        ''"Initial Catalog=Laps;Data Source=172.27.63.16;User ID=SA;Password=u3zD2Ix6;Connect Timeout=0;Connection Timeout=0;Integrated Security=SSPI;"
        ''"Provider=SQLOLEDB.1;Persist Security Info=false;Initial Catalog=LAPS;Data Source=172.27.63.16;User ID=SA;Password=u3zD2Ix6"
        ''"Initial Catalog=GDSreporting;Data Source=172.27.63.16;User ID=Unclebuck\Integrator;Password=3-y+ZLsD;Connect Timeout=0;Connection Timeout=0;Integrated Security=SSPI;"
        ''46.37.173.116


        'If connType = "live" Then
        '    connString = "Initial Catalog=LAPS;Data Source=172.27.63.23;User ID=SA;Password=u3zD2Ix6;Connect Timeout=0;Connection Timeout=0;Integrated Security=SSPI;"
        '    '"Initial Catalog=LAPS;Data Source=172.27.63.23;User ID=Unclebuck\Integrator;Password=3-y+ZLsD;Connect Timeout=0;Connection Timeout=0;Integrated Security=SSPI;"
        'ElseIf connType = "test" Then
        '    connString = "Initial Catalog=LAPS_UAT;Data Source=172.27.63.53;User ID=LUATREAD;Password=AF#8csV6;Connect Timeout=0;Connection Timeout=0;Integrated Security=False;"
        '    '"Initial Catalog=LAPS_UAT;Data Source=172.27.63.53;User ID=LUATREAD;Password=AF#8csV6;Connect Timeout=0;Connection Timeout=0;Integrated Security=False;"
        'End If


        Dim dataSource = "ub-avgr01.unclebuck.ukfast"
        ''''Dim user = "Integrator"
        Dim user = "Integrator2"
        Dim pw = "ss%$W£$%09-0-!"

        'Dim dataSource = "172.27.63.23"
        'Dim user = "sa"
        'Dim pw = "u3zD2Ix6"

        Dim connString = "Initial Catalog=Automation;Data Source=" & dataSource & ";User ID=" & user & ";Password=" & pw & ";Connect Timeout=0;Connection Timeout=0;Integrated Security=False;"
        'MsgBox(connString)

        Return connString
    End Function

    Function sqlToDataTable(ByVal sqlQuery As String, ByVal connStr As String) As DataTable
        Dim myConn As SqlConnection
        Dim myCmd As SqlCommand
        Dim myReader As SqlDataReader
        Dim results As New DataTable

        Try
            'MsgBox("In sqlToDataTable")
            myConn = New SqlConnection(connStr)
            myCmd = myConn.CreateCommand
            'MsgBox("Created SqlConnection")
            myCmd.CommandTimeout = 0
            myCmd.CommandText = sqlQuery

            myConn.Open()
            'MsgBox("Connection open")
            myReader = myCmd.ExecuteReader()

            results.Load(myReader)
            'MsgBox("db data retrieved")

            myReader.Close()
            myReader = Nothing
            myConn.Close()
            myConn = Nothing
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        'MsgBox("Returning db data")
        Return results
    End Function
End Module
