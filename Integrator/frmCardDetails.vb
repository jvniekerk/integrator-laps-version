﻿Public Class frmCardDetails
    'Private Sub frmCardDetails_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
    '    'If MsgBox("Are you sure you want to returnn to the menu?", vbYesNo + vbQuestion, "Exit requested") <> 6 Then
    '    '    e.Cancel = True
    '    'Else
    '    Close()
    '    frmMenu.Show()
    '    'End If

    '    'Application.Exit()
    'End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Close()
        frmMenu.Show()
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        Dim emailNo As String = txtEmailNo.Text.ToString

        If CheckValidNumber(emailNo) = False Then
            MsgBox("Invalid email number. Please try again.", vbOKOnly + vbCritical, "Invalid email number")
        Else
            Call DeleteCardDetails(emailNo)
        End If
    End Sub
End Class