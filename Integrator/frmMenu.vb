﻿Public Class frmMenu
    Dim username As String
    Dim password As String
    'Private Sub frmMenu_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
    '    'If MsgBox("Are you sure you want to exit?", vbYesNo + vbQuestion, "Exit requested") <> 6 Then
    '    '    e.Cancel = True
    '    'Else
    '    '    Application.Exit()
    '    'End If

    '    Application.Exit()
    'End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        If MsgBox("Are you sure you want to exit?", vbYesNo + vbQuestion, "Exit requested") = 6 Then
            Close()
            Form1.Close()
        End If
    End Sub

    Private Sub frmMenu_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        username = lblUsername.Text
        password = lblPassword.Text

        'MsgBox("username: """ & username.ToString & """; password: """ & password.ToString & """")
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        Dim form As String = ""
        Dim selected As Boolean = True

        For Each rdb As Object In Controls
            If TypeOf rdb Is RadioButton Then
                If rdb.Checked = True Then
                    form = rdb.Name
                End If
            End If
        Next rdb

        Select Case form
            Case "rdbDeleteCardDetails"
                frmCardDetails.Show()
            Case "rdbDeleteCardImage"
                frmCardImages.Show()
            Case "rdbCreditReport"
                frmCreditReport.Show()
            Case Else
                selected = False
                MsgBox("No option selected, please select a function to run.", vbOKOnly + vbCritical, "Please select a function")
        End Select

        If selected Then
            Hide()
        End If
    End Sub
End Class