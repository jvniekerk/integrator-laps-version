﻿Public Class frmCardImages
    'Private Sub frmCardImages_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
    '    'If MsgBox("Are you sure you want to returnn to the menu?", vbYesNo + vbQuestion, "Exit requested") <> 6 Then
    '    '    e.Cancel = True
    '    'Else
    '    Close()
    '    frmMenu.Show()
    '    'End If

    '    'Application.Exit()
    'End Sub
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Close()
        frmMenu.Show()
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        Dim emailNo As String = txtEmailNo.Text.ToString
        Dim image As String = txtAttachment.Text.ToString

        If CheckValidNumber(emailNo) = False Then
            MsgBox("Invalid email number. Please try again.", vbOKOnly + vbCritical, "Invalid email number")
        ElseIf CheckValidInput(image) = False Then
            MsgBox("Invalid attachment name. Please try again.", vbOKOnly + vbCritical, "Invalid attachment name")
        ElseIf image.IndexOf(".") = 0 Then
            MsgBox("Invalid attachment name. Please add the file extension of the file too.", vbOKOnly + vbCritical, "Invalid attachment name")
        Else
            Call DeleteCardImage(emailNo, image)
        End If
    End Sub
End Class