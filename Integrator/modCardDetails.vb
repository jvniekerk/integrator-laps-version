﻿Module modCardDetails
    Sub DeleteCardDetails(ByVal thisEmailNo As String)
        Dim query As String = "select count(EmailNo) from laps.dbo.email where EmailNo = " & thisEmailNo

        If MsgBox("Are you sure you want to clear the selected email body?", vbQuestion + vbYesNo, "Clear Email Body") = 6 Then
            Dim thisTable As DataTable = sqlToDataTable(query, GetConnectionString("live"))
            Dim rows As Integer = thisTable.Rows.Count
            thisTable.Clear()

            If rows > 0 Then
                query = "declare @EmailNo varchar(12) "
                query += "set @EmailNo = '' "
                query += "update LAPS.dbo.Email set Eml_BodyHtml = 'Cleared due to card details in email body' "
                query += ", Eml_BodyText = 'Cleared due to card details in email body' where EmailNo = @EmailNo "
                query += "insert into JosefTestingDB.dbo.EmailBodyCleared ([Ebc_EmailNo], [Ebc_Reason]) values (@EmailNo, 12) "
                thisTable = sqlToDataTable(query, GetConnectionString("live"))
                thisTable.Clear()

                MsgBox("Email body cleared successfully.", vbInformation + vbOKOnly, "Email body cleared")
            Else
                MsgBox("Email not found on the database. Please try again.", vbCritical + vbOKOnly, "Email not found")
            End If
        End If
    End Sub

    Sub DeleteCardImage(ByVal thisEmailNo As String, ByVal fileName As String)
        Dim query As String = "select EmailID from laps.dbo.email where EmailNo = " & thisEmailNo
        Dim thisTable As DataTable = sqlToDataTable(query, GetConnectionString("live"))
        Dim rows As Integer = thisTable.Rows.Count
        thisTable.Clear()

        If rows > 0 Then
            query = "declare @emailNo int = " & thisEmailNo.ToString & " , @attachment varchar(max) = '" & fileName.ToString & "' "
            query += "declare @emailID uniqueidentifier = (select EmailID from Laps.dbo.Email where EmailNo = @emailNo) "
            query += "select [FileName] = '\\46.37.173.115\D:\Laps\Production\Files\Emails\' + lower(@emailID) + '\' + lower(a.EmailAttachmentID) "
            'query += "select [FileName] = 'D:\Laps\Production\Files\Emails\' + lower(@emailID) + '\' "
            'query += " From Laps.dbo.EmailAttachment a where a.Ema_EmailID = @emailID "
            'query += "select [FileName] = '\\D:\Laps\Production\Files\Emails\' + lower(@emailID) + '\' + lower(a.EmailAttachmentID) "
            query += "+ '.' + a.Ema_FileType From Laps.dbo.EmailAttachment a where a.Ema_EmailID = @emailID "
            query += "and lower(a.Ema_FileName) = lower(@attachment) "

            thisTable = sqlToDataTable(query, GetConnectionString("live"))
            rows = thisTable.Rows.Count

            If rows > 0 Then
                fileName = thisTable.Rows(0).Item(0)
                fileName = "\\46.37.173.115\"   'C:\
                MsgBox("filename: """ & fileName & """")    '"""; dir: """ & Dir(fileName, vbDirectory) & """")

                'Dim Impersonator As New clsImpersonator("UNCLEBUCK\jvniekerk", "B00m4p13")
                'Impersonator.BeginImpersonation()
                Dim Impersonator As New clsAuthenticator
                Dim sDomain As String = "46.37.173.115"
                Dim sUser As String = "unclebuck\jvniekerk"
                Dim sPass As String = "B00m4p13"
                Impersonator.Impersonator(sDomain, sUser, sPass)

                If IO.Directory.Exists(fileName) Then
                    'If IO.File.Exists(fileName) Then
                    MsgBox("file exists")
                Else
                    MsgBox("couldn't find file")
                    End If

                    'Impersonator.EndImpersonation()
                    Impersonator.Undo()
                Else
                    MsgBox("Attachment not found on the database. Please try again.", vbOKOnly + vbCritical, "Attachment not found")
            End If
        Else
            MsgBox("Email not found on the database. Please try again.", vbCritical + vbOKOnly, "Email not found")
        End If
    End Sub
End Module
