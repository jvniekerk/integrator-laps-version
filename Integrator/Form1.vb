﻿Public Class Form1
    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        If MsgBox("Are you sure you want to exit?", vbYesNo + vbQuestion, "Exit requested") = 6 Then
            Close()
        End If
    End Sub

    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        Dim username As String = txtUsername.Text.ToString
        Dim password As String = txtPassword.Text.ToString
        Dim query As String

        'Call test()

        If CheckValidInput(username) = False Then
            MsgBox("Invalid username. Please rety.", vbOKOnly + vbCritical, "Invalid username")
        ElseIf CheckValidInput(password) = False Then
            MsgBox("Invalid password. Please rety.", vbOKOnly + vbCritical, "Invalid password")
        Else
            query = "declare @un varchar(100) = '" & username.ToString & "', @pw varchar(100) = '" & password.ToString & "' "
            query += "select count(*) as Matches from Automation.dbo.Integrator_Users u "
            query += "where lower(u.Username) = lower(@un) and lower(u.[Password]) = lower(@pw) and u.Active =1"
            'MsgBox(query)

            Dim thisTable As DataTable = sqlToDataTable(query, GetConnectionString())
            Dim matches As String = thisTable.Rows(0).Item(0).ToString
            'MsgBox(matches)

            If Double.Parse(matches) = 0 Then
                MsgBox("Incorrect username & password combination. Please try again.", vbOKOnly + vbCritical, "Incorrect username & password combination")
            Else
                With frmMenu
                    .lblUsername.Text = username
                    .lblPassword.Text = password
                    .Show()
                End With

                Hide()
            End If
        End If
    End Sub

    Sub test()
        Dim query As String = "select top 1 LoanNo from laps_uat.dbo.loan"
        Dim thisTable As DataTable = sqlToDataTable(query, GetConnectionString("test"))
        MsgBox(thisTable.Rows.Count)

        query = "select top 1 LoanNo from laps.dbo.loan"
        thisTable = sqlToDataTable(query, GetConnectionString("live"))
        MsgBox(thisTable.Rows.Count)
    End Sub
End Class
