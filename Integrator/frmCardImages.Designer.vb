﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCardImages
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnRun = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.txtEmailNo = New System.Windows.Forms.TextBox()
        Me.lblEmailNo = New System.Windows.Forms.Label()
        Me.lblAttachment = New System.Windows.Forms.Label()
        Me.txtAttachment = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'btnRun
        '
        Me.btnRun.Location = New System.Drawing.Point(110, 176)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(165, 70)
        Me.btnRun.TabIndex = 13
        Me.btnRun.Text = "Delete Email Body"
        Me.btnRun.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(375, 176)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(165, 70)
        Me.btnExit.TabIndex = 12
        Me.btnExit.Text = "Return to Menu"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'txtEmailNo
        '
        Me.txtEmailNo.Location = New System.Drawing.Point(321, 34)
        Me.txtEmailNo.Name = "txtEmailNo"
        Me.txtEmailNo.Size = New System.Drawing.Size(100, 31)
        Me.txtEmailNo.TabIndex = 0
        Me.txtEmailNo.Text = "282254"
        '
        'lblEmailNo
        '
        Me.lblEmailNo.AutoSize = True
        Me.lblEmailNo.Location = New System.Drawing.Point(105, 34)
        Me.lblEmailNo.Name = "lblEmailNo"
        Me.lblEmailNo.Size = New System.Drawing.Size(209, 25)
        Me.lblEmailNo.TabIndex = 10
        Me.lblEmailNo.Text = "Enter Email Number:"
        '
        'lblAttachment
        '
        Me.lblAttachment.AutoSize = True
        Me.lblAttachment.Location = New System.Drawing.Point(105, 90)
        Me.lblAttachment.Name = "lblAttachment"
        Me.lblAttachment.Size = New System.Drawing.Size(435, 25)
        Me.lblAttachment.TabIndex = 14
        Me.lblAttachment.Text = "Please enter the attachment name to delete:"
        '
        'txtAttachment
        '
        Me.txtAttachment.Location = New System.Drawing.Point(110, 119)
        Me.txtAttachment.Name = "txtAttachment"
        Me.txtAttachment.Size = New System.Drawing.Size(430, 31)
        Me.txtAttachment.TabIndex = 1
        Me.txtAttachment.Text = "IMG_1029.JPG"
        '
        'frmCardImages
        '
        Me.AcceptButton = Me.btnRun
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(642, 271)
        Me.Controls.Add(Me.txtAttachment)
        Me.Controls.Add(Me.lblAttachment)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.txtEmailNo)
        Me.Controls.Add(Me.lblEmailNo)
        Me.Name = "frmCardImages"
        Me.Text = "Delete Card Image"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnRun As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents txtEmailNo As TextBox
    Friend WithEvents lblEmailNo As Label
    Friend WithEvents lblAttachment As Label
    Friend WithEvents txtAttachment As TextBox
End Class
