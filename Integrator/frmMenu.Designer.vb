﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.lblUsername = New System.Windows.Forms.Label()
        Me.lblPassword = New System.Windows.Forms.Label()
        Me.btnRun = New System.Windows.Forms.Button()
        Me.rdbChangePW = New System.Windows.Forms.RadioButton()
        Me.rdbDeleteCardDetails = New System.Windows.Forms.RadioButton()
        Me.rdbDeleteCardImage = New System.Windows.Forms.RadioButton()
        Me.rdbCreditReport = New System.Windows.Forms.RadioButton()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(472, 262)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(133, 62)
        Me.btnExit.TabIndex = 7
        Me.btnExit.Text = "Exit"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'lblUsername
        '
        Me.lblUsername.AutoSize = True
        Me.lblUsername.Location = New System.Drawing.Point(21, 9)
        Me.lblUsername.Name = "lblUsername"
        Me.lblUsername.Size = New System.Drawing.Size(0, 25)
        Me.lblUsername.TabIndex = 8
        Me.lblUsername.Visible = False
        '
        'lblPassword
        '
        Me.lblPassword.AutoSize = True
        Me.lblPassword.Location = New System.Drawing.Point(27, 9)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(0, 25)
        Me.lblPassword.TabIndex = 9
        Me.lblPassword.Visible = False
        '
        'btnRun
        '
        Me.btnRun.Location = New System.Drawing.Point(26, 262)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(133, 62)
        Me.btnRun.TabIndex = 10
        Me.btnRun.Text = "Run"
        Me.btnRun.UseVisualStyleBackColor = True
        '
        'rdbChangePW
        '
        Me.rdbChangePW.AutoSize = True
        Me.rdbChangePW.Enabled = False
        Me.rdbChangePW.Location = New System.Drawing.Point(384, 134)
        Me.rdbChangePW.Name = "rdbChangePW"
        Me.rdbChangePW.Size = New System.Drawing.Size(218, 29)
        Me.rdbChangePW.TabIndex = 11
        Me.rdbChangePW.Text = "Change Password"
        Me.rdbChangePW.UseVisualStyleBackColor = True
        '
        'rdbDeleteCardDetails
        '
        Me.rdbDeleteCardDetails.AutoSize = True
        Me.rdbDeleteCardDetails.Enabled = False
        Me.rdbDeleteCardDetails.Location = New System.Drawing.Point(38, 78)
        Me.rdbDeleteCardDetails.Name = "rdbDeleteCardDetails"
        Me.rdbDeleteCardDetails.Size = New System.Drawing.Size(229, 29)
        Me.rdbDeleteCardDetails.TabIndex = 12
        Me.rdbDeleteCardDetails.Text = "Delete Card Details"
        Me.rdbDeleteCardDetails.UseVisualStyleBackColor = True
        '
        'rdbDeleteCardImage
        '
        Me.rdbDeleteCardImage.AutoSize = True
        Me.rdbDeleteCardImage.Enabled = False
        Me.rdbDeleteCardImage.Location = New System.Drawing.Point(38, 134)
        Me.rdbDeleteCardImage.Name = "rdbDeleteCardImage"
        Me.rdbDeleteCardImage.Size = New System.Drawing.Size(221, 29)
        Me.rdbDeleteCardImage.TabIndex = 13
        Me.rdbDeleteCardImage.Text = "Delete Card Image"
        Me.rdbDeleteCardImage.UseVisualStyleBackColor = True
        '
        'rdbCreditReport
        '
        Me.rdbCreditReport.AutoSize = True
        Me.rdbCreditReport.Checked = True
        Me.rdbCreditReport.Location = New System.Drawing.Point(384, 78)
        Me.rdbCreditReport.Name = "rdbCreditReport"
        Me.rdbCreditReport.Size = New System.Drawing.Size(221, 29)
        Me.rdbCreditReport.TabIndex = 14
        Me.rdbCreditReport.TabStop = True
        Me.rdbCreditReport.Text = "Get Credit Reports"
        Me.rdbCreditReport.UseVisualStyleBackColor = True
        '
        'frmMenu
        '
        Me.AcceptButton = Me.btnRun
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(636, 355)
        Me.Controls.Add(Me.rdbCreditReport)
        Me.Controls.Add(Me.rdbDeleteCardImage)
        Me.Controls.Add(Me.rdbDeleteCardDetails)
        Me.Controls.Add(Me.rdbChangePW)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.lblPassword)
        Me.Controls.Add(Me.lblUsername)
        Me.Controls.Add(Me.btnExit)
        Me.Name = "frmMenu"
        Me.Text = "Integrator Menu"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnExit As Button
    Friend WithEvents lblUsername As Label
    Friend WithEvents lblPassword As Label
    Friend WithEvents btnRun As Button
    Friend WithEvents rdbChangePW As RadioButton
    Friend WithEvents rdbDeleteCardDetails As RadioButton
    Friend WithEvents rdbDeleteCardImage As RadioButton
    Friend WithEvents rdbCreditReport As RadioButton
End Class
