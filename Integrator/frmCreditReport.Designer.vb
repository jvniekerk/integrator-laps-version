﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCreditReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.chkSHARE = New System.Windows.Forms.CheckBox()
        Me.chkTAC = New System.Windows.Forms.CheckBox()
        Me.chkCV = New System.Windows.Forms.CheckBox()
        Me.btnRun = New System.Windows.Forms.Button()
        Me.lblReport = New System.Windows.Forms.Label()
        Me.lblLoanNo = New System.Windows.Forms.Label()
        Me.txtLoanNo = New System.Windows.Forms.TextBox()
        Me.chkTT = New System.Windows.Forms.CheckBox()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(335, 319)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(165, 70)
        Me.btnExit.TabIndex = 9
        Me.btnExit.Text = "Return to Menu"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'chkSHARE
        '
        Me.chkSHARE.AutoSize = True
        Me.chkSHARE.Checked = True
        Me.chkSHARE.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSHARE.Location = New System.Drawing.Point(46, 160)
        Me.chkSHARE.Name = "chkSHARE"
        Me.chkSHARE.Size = New System.Drawing.Size(116, 29)
        Me.chkSHARE.TabIndex = 10
        Me.chkSHARE.Text = "SHARE"
        Me.chkSHARE.UseVisualStyleBackColor = True
        '
        'chkTAC
        '
        Me.chkTAC.AutoSize = True
        Me.chkTAC.Checked = True
        Me.chkTAC.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTAC.Location = New System.Drawing.Point(46, 207)
        Me.chkTAC.Name = "chkTAC"
        Me.chkTAC.Size = New System.Drawing.Size(86, 29)
        Me.chkTAC.TabIndex = 11
        Me.chkTAC.Text = "TAC"
        Me.chkTAC.UseVisualStyleBackColor = True
        '
        'chkCV
        '
        Me.chkCV.AutoSize = True
        Me.chkCV.Location = New System.Drawing.Point(335, 160)
        Me.chkCV.Name = "chkCV"
        Me.chkCV.Size = New System.Drawing.Size(165, 29)
        Me.chkCV.TabIndex = 12
        Me.chkCV.Text = "Call Validate"
        Me.chkCV.UseVisualStyleBackColor = True
        '
        'btnRun
        '
        Me.btnRun.Location = New System.Drawing.Point(46, 319)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(165, 70)
        Me.btnRun.TabIndex = 13
        Me.btnRun.Text = "Get Report(s)"
        Me.btnRun.UseVisualStyleBackColor = True
        '
        'lblReport
        '
        Me.lblReport.AutoSize = True
        Me.lblReport.Location = New System.Drawing.Point(41, 114)
        Me.lblReport.Name = "lblReport"
        Me.lblReport.Size = New System.Drawing.Size(459, 25)
        Me.lblReport.TabIndex = 14
        Me.lblReport.Text = "Select the Call Credit report(s) you want to get:"
        '
        'lblLoanNo
        '
        Me.lblLoanNo.AutoSize = True
        Me.lblLoanNo.Location = New System.Drawing.Point(41, 47)
        Me.lblLoanNo.Name = "lblLoanNo"
        Me.lblLoanNo.Size = New System.Drawing.Size(93, 25)
        Me.lblLoanNo.TabIndex = 15
        Me.lblLoanNo.Text = "LoanNo:"
        '
        'txtLoanNo
        '
        Me.txtLoanNo.Location = New System.Drawing.Point(140, 47)
        Me.txtLoanNo.Name = "txtLoanNo"
        Me.txtLoanNo.Size = New System.Drawing.Size(100, 31)
        Me.txtLoanNo.TabIndex = 16
        '
        'chkTT
        '
        Me.chkTT.AutoSize = True
        Me.chkTT.Location = New System.Drawing.Point(335, 207)
        Me.chkTT.Name = "chkTT"
        Me.chkTT.Size = New System.Drawing.Size(141, 29)
        Me.chkTT.TabIndex = 17
        Me.chkTT.Text = "True Time"
        Me.chkTT.UseVisualStyleBackColor = True
        '
        'frmCreditReport
        '
        Me.AcceptButton = Me.btnRun
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(564, 431)
        Me.Controls.Add(Me.chkTT)
        Me.Controls.Add(Me.txtLoanNo)
        Me.Controls.Add(Me.lblLoanNo)
        Me.Controls.Add(Me.lblReport)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.chkCV)
        Me.Controls.Add(Me.chkTAC)
        Me.Controls.Add(Me.chkSHARE)
        Me.Controls.Add(Me.btnExit)
        Me.Name = "frmCreditReport"
        Me.Text = "Credit Report"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnExit As Button
    Friend WithEvents chkSHARE As CheckBox
    Friend WithEvents chkTAC As CheckBox
    Friend WithEvents chkCV As CheckBox
    Friend WithEvents btnRun As Button
    Friend WithEvents lblReport As Label
    Friend WithEvents lblLoanNo As Label
    Friend WithEvents txtLoanNo As TextBox
    Friend WithEvents chkTT As CheckBox
End Class
