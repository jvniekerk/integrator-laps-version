﻿Option Explicit On

Module modFunctions
    Function CheckValidInput(ByVal strToCheck As String) As Boolean
        Dim outcome As Boolean = True

        If Len(outcome) = 0 Then
            outcome = False
        End If

        Return outcome
    End Function

    Function CheckValidNumber(ByVal thisNumber As String) As Boolean
        Dim result As Boolean = True
        Dim testVal As Integer

        If Not Integer.TryParse(thisNumber, testVal) Then
            result = False
        End If

        If result Then
            If Len(thisNumber) < 2 Or thisNumber < 0 Then
                result = False
            End If
        End If

        Return result
    End Function
End Module
