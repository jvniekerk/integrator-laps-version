﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCardDetails
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblEmailNo = New System.Windows.Forms.Label()
        Me.txtEmailNo = New System.Windows.Forms.TextBox()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnRun = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lblEmailNo
        '
        Me.lblEmailNo.AutoSize = True
        Me.lblEmailNo.Location = New System.Drawing.Point(44, 30)
        Me.lblEmailNo.Name = "lblEmailNo"
        Me.lblEmailNo.Size = New System.Drawing.Size(209, 25)
        Me.lblEmailNo.TabIndex = 0
        Me.lblEmailNo.Text = "Enter Email Number:"
        '
        'txtEmailNo
        '
        Me.txtEmailNo.Location = New System.Drawing.Point(260, 30)
        Me.txtEmailNo.Name = "txtEmailNo"
        Me.txtEmailNo.Size = New System.Drawing.Size(100, 31)
        Me.txtEmailNo.TabIndex = 0
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(227, 110)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(165, 70)
        Me.btnExit.TabIndex = 8
        Me.btnExit.Text = "Return to Menu"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnRun
        '
        Me.btnRun.Location = New System.Drawing.Point(49, 110)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(165, 70)
        Me.btnRun.TabIndex = 9
        Me.btnRun.Text = "Delete Email Body"
        Me.btnRun.UseVisualStyleBackColor = True
        '
        'frmCardDetails
        '
        Me.AcceptButton = Me.btnRun
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(412, 217)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.txtEmailNo)
        Me.Controls.Add(Me.lblEmailNo)
        Me.Name = "frmCardDetails"
        Me.Text = "Delete Card Details from Email"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblEmailNo As Label
    Friend WithEvents txtEmailNo As TextBox
    Friend WithEvents btnExit As Button
    Friend WithEvents btnRun As Button
End Class
