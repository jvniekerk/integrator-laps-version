﻿Imports System.IO

Public Class frmCreditReport
    'Private Sub frmCreditReport_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
    '    'If MsgBox("Are you sure you want to return to the menu?", vbYesNo + vbQuestion, "Exit requested") <> 6 Then
    '    '    e.Cancel = True
    '    'Else
    '    Close()
    '    frmMenu.Show()
    '    'End If

    '    'Application.Exit()
    'End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Close()
        frmMenu.Show()
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        Dim loanNo As String = txtLoanNo.Text.ToString
        Dim timeStamp As String = Replace(Replace(Replace(Now().ToString, "/", ""), " ", "_"), ":", "")
        Dim fileName As String = "CallCredit_" & loanNo.ToString & "_" & timeStamp
        Dim path As String = "C:\"
        Dim selected As Boolean = False
        Dim reports As Integer()
        ReDim reports(3)

        For i As Integer = 0 To 3
            reports(i) = 0
        Next i

        If chkSHARE.Checked Then
            reports(0) = 1
        End If
        If chkTAC.Checked Then
            reports(1) = 1
        End If
        If chkCV.Checked Then
            reports(2) = 1
        End If
        If chkTT.Checked Then
            reports(3) = 1
        End If

        For i = 0 To 3
            If reports(i) = 1 Then
                selected = True
            End If
        Next i

        If selected Then
            If CheckValidNumber(loanNo) Then
                Dim query As String
                Dim dialog As New FolderBrowserDialog()
                Dim request As Boolean = True

                dialog.RootFolder = Environment.SpecialFolder.Desktop
                dialog.SelectedPath = path
                dialog.Description = "Select a folder to save file(s) in"

                If dialog.ShowDialog() = DialogResult.OK Then
                    path = dialog.SelectedPath
                Else
                    query = "No folder selected. Do you want to save your file(s) in the folder """ & path.ToString & """?"
                    Dim result As Integer = MsgBox(query, vbYesNo + vbQuestion, "No destination folder selected")

                    If result <> DialogResult.Yes Then
                        request = False
                    End If
                End If

                If request Then
                    Call GetCreditReports(reports, loanNo, path, fileName)
                End If

                dialog.Dispose()
            Else
                MsgBox("Invalid LoanNo. Please enter a valid LoanNo", vbOKOnly + vbCritical, "Invalid LoanNo")
            End If
        Else
            MsgBox("No Report Selected. Please select at least one report to search for.", vbOKOnly + vbCritical, "CRA Reports")
        End If
    End Sub

    Sub GetCreditReports(ByVal reports As Integer(), ByVal LoanNo As String, ByVal path As String, ByVal fileName As String)
        Dim query As String = " exec Automation.dbo.sp_GetCallCreditReports " & LoanNo.ToString
        Dim thisFile As String = path & "\" & fileName
        Dim length As Integer = reports.Length
        'MsgBox("Length: " & length.ToString)

        frmWait.Show()
        frmWait.lblWait.Refresh()
        Hide()

        For i As Integer = 0 To length - 1
            query += " , " & reports(i).ToString
        Next i
        'MsgBox(query)

        Dim thisTable As DataTable = sqlToDataTable(query, GetConnectionString())
        Dim rows As Integer = thisTable.Rows.Count
        Dim extension As String
        Dim report As String
        Dim colName As String
        Dim columns As Integer = thisTable.Columns.Count

        frmWait.Close()
        Show()

        If columns = 0 Then
            MsgBox("No reports found.", vbOKOnly + vbInformation, "No reports found")
        ElseIf columns < length Then
            Dim returned() As String
            ReDim returned(columns - 1)
            'MsgBox("columns < length")

            For i = 0 To columns - 1
                returned(i) = Microsoft.VisualBasic.Left(thisTable.Columns(i).ColumnName, thisTable.Columns(i).ColumnName.IndexOf("_"))
                'MsgBox("Header: " & returned(i))
            Next i

            For i = 0 To length - 1
                Dim thisReport As String = ""

                Select Case i
                    Case 0
                        thisReport = "SHARE"
                    Case 1
                        thisReport = "TAC"
                    Case 2
                        thisReport = "CallVal"
                    Case 3
                        thisReport = "TrueTime"
                End Select
                'MsgBox(thisReport & " " & reports(i).ToString)

                If reports(i) = 1 Then
                    Dim isReturn As Boolean = False

                    For j As Integer = 0 To columns - 1
                        If returned(j).ToString = thisReport Then
                            isReturn = True
                        End If
                    Next j
                    'MsgBox(isReturn)

                    If isReturn = False Then
                        MsgBox("Report """ & thisReport & """ not found.", vbOKOnly + vbInformation, "Report not found")
                    End If
                End If
            Next i
        End If

        If rows > 0 Then
            Dim thisData As String

            For i = 0 To columns - 1
                colName = thisTable.Columns(i).ColumnName
                thisData = thisTable.Rows(0).Item(i).ToString

                If Len(thisData) > 1 Then
                    Dim searchString As String = Microsoft.VisualBasic.Left(thisData, 40)
                    'MsgBox(searchString & " -> " & searchString.IndexOf("?xml version=").ToString)
                    If searchString.IndexOf("?xml version=") >= 0 Then
                        'MsgBox("replacing caps")
                        'thisData.Replace("UTF-16", "UTF-8")
                        'MsgBox("replacing small")

                        'If thisData.IndexOf("utf-") >= 0 Then
                        '    MsgBox(Microsoft.VisualBasic.Left(thisData, 40))
                        'Else
                        '    MsgBox("utf not found")
                        'End If

                        'thisData.Replace("utf-16", "utf-8")
                        'thisData.Replace("utf-8", "utf-16")
                    Else
                        'MsgBox("not replacing")
                        thisData = "<?xml version=""1.0"" encoding=""UTF-8""?>" & thisData
                    End If
                    'MsgBox(Microsoft.VisualBasic.Left(thisData, 100))

                    'thisData = "<?xml version=""1.0"" encoding=""UTF-8""?>" & thisData
                    extension = Microsoft.VisualBasic.Right(colName, 3)
                    report = "_" & Microsoft.VisualBasic.Left(colName, Len(colName) - 4)
                    thisFile = path & "\" & fileName & report.ToString & "." & extension.ToString

                    Using outputFile As New StreamWriter(thisFile)
                        outputFile.WriteLine(thisData)
                    End Using

                    MsgBox("File: """ & thisFile & """ saved.")
                Else
                    report = Microsoft.VisualBasic.Left(thisTable.Columns(i).ColumnName, thisTable.Columns(i).ColumnName.IndexOf("_"))
                    MsgBox("Report """ & report & """ not found.", vbOKOnly + vbInformation, "Report not found")
                End If
            Next i
        End If
    End Sub
End Class